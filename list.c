#include "list.h"

void cds_list_add(struct cds_list_head *newp, struct cds_list_head *head);
void cds_list_add_tail(struct cds_list_head *newp, struct cds_list_head *head);
void __cds_list_del(struct cds_list_head *prev, struct cds_list_head *next);
void cds_list_del(struct cds_list_head *elem);
void cds_list_del_init(struct cds_list_head *elem);
void cds_list_move(struct cds_list_head *elem, struct cds_list_head *head);
void cds_list_replace(struct cds_list_head *old, struct cds_list_head *_new);
void cds_list_splice(struct cds_list_head *add, struct cds_list_head *head);
int cds_list_empty(struct cds_list_head *head);
void cds_list_replace_init(struct cds_list_head *old,
                           struct cds_list_head *_new);
