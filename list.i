# 0 "list.c"
# 0 "<built-in>"
# 0 "<command-line>"
# 1 "/usr/include/stdc-predef.h" 1 3 4
# 0 "<command-line>" 2
# 1 "list.c"
# 1 "list.h" 1
# 22 "list.h"
struct cds_list_head {
 struct cds_list_head *next, *prev;
};
# 38 "list.h"
void cds_list_add(struct cds_list_head *newp, struct cds_list_head *head)
{
 head->next->prev = newp;
 newp->next = head->next;
 newp->prev = head;
 head->next = newp;
}



void cds_list_add_tail(struct cds_list_head *newp, struct cds_list_head *head)
{
 head->prev->next = newp;
 newp->next = head;
 newp->prev = head->prev;
 head->prev = newp;
}



void __cds_list_del(struct cds_list_head *prev, struct cds_list_head *next)
{
 next->prev = prev;
 prev->next = next;
}



void cds_list_del(struct cds_list_head *elem)
{
 __cds_list_del(elem->prev, elem->next);
}



void cds_list_del_init(struct cds_list_head *elem)
{
 cds_list_del(elem);
 (elem)->next = (elem)->prev = (elem);
}



void cds_list_move(struct cds_list_head *elem, struct cds_list_head *head)
{
 __cds_list_del(elem->prev, elem->next);
 cds_list_add(elem, head);
}



void cds_list_replace(struct cds_list_head *old, struct cds_list_head *_new)
{
 _new->next = old->next;
 _new->prev = old->prev;
 _new->prev->next = _new;
 _new->next->prev = _new;
}



void cds_list_splice(struct cds_list_head *add, struct cds_list_head *head)
{

 if (add != add->next) {
  add->next->prev = head;
  add->prev->next = head->next;
  head->next->prev = add->prev;
  head->next = add->next;
 }
}
# 170 "list.h"
int cds_list_empty(struct cds_list_head *head)
{
 return head == head->next;
}


void cds_list_replace_init(struct cds_list_head *old,
  struct cds_list_head *_new)
{
 struct cds_list_head *head = old->next;

 cds_list_del(old);
 cds_list_add_tail(_new, head);
 (old)->next = (old)->prev = (old);
}
# 2 "list.c" 2

void cds_list_add(struct cds_list_head *newp, struct cds_list_head *head);
void cds_list_add_tail(struct cds_list_head *newp, struct cds_list_head *head);
void __cds_list_del(struct cds_list_head *prev, struct cds_list_head *next);
void cds_list_del(struct cds_list_head *elem);
void cds_list_del_init(struct cds_list_head *elem);
void cds_list_move(struct cds_list_head *elem, struct cds_list_head *head);
void cds_list_replace(struct cds_list_head *old, struct cds_list_head *_new);
void cds_list_splice(struct cds_list_head *add, struct cds_list_head *head);
int cds_list_empty(struct cds_list_head *head);
void cds_list_replace_init(struct cds_list_head *old,
                           struct cds_list_head *_new);
