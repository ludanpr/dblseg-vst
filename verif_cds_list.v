(** Verify *)
Require Import VST.floyd.proofauto.
Require Import VC.list.
#[export] Instance CompSpecs : compspecs. make_compspecs prog. Defined.
Definition Vprog : varspecs. mk_varspecs prog. Defined.

Require Export Coq.Logic.Classical.

(** `struct cds_list_head` abbreviation.
 *)
Definition t_cds_list := Tstruct _cds_list_head noattr.

(** A separation-logic predicate to describe the concept that the address `p` in memory
 is a doubly-linked list, `q` is its tail. `h` is the head of the concept list. `p = next`
 and `q = prev`, then the list is empty; otherwise, there exists a cons cell at address `h`
 with components (p', prev). Inductively, at address `p'`, there is the representation of
 the rest of the list, `dlseg hs p' q next p`.
 *)

Fixpoint dlseg (sigma : list val) (p q next prev : val) : mpred :=
  match sigma with
  | h :: hs => EX p' : val, !!(p = h) && data_at Tsh t_cds_list (p', prev) p * dlseg hs p' q next p
  | nil => !!(p = next /\ q = prev) && emp
  end.

(* Singleton in doubly-linked segment *)
Lemma dlseg_singleton : forall h head tail next prev,
    dlseg [h] head tail next prev |-- !!(head = tail /\ head = h) &&
      data_at Tsh t_cds_list (next, prev) h.
Proof.
  intros.
  unfold dlseg.
  Intros p'.
  entailer!. Qed.

(* Element to the left doubly-linked segment *)
Lemma dlseg_left_elem : forall sigma h head tail next prev,
    dlseg (h :: sigma) head tail next prev |--
      EX next', !!(head = h) && emp *
                  dlseg sigma next' tail next h *
                  data_at Tsh t_cds_list (next', prev) h.
Proof.
  intros.
  destruct sigma.
  - sep_apply dlseg_singleton.
    unfold dlseg; fold dlseg.
    Exists next.
    entailer!.
  - unfold dlseg; fold dlseg.
    Intros head'.
    Exists head'.
    entailer!. Qed.

Lemma elem_left_dlseg : forall sigma h head tail next prev next',
    !!(head = h) && emp *
      dlseg sigma next' tail next h *
      data_at Tsh t_cds_list (next', prev) h |--
              dlseg (h :: sigma) head tail next prev.
Proof.
  intros.
  entailer!.
  destruct sigma.
  - unfold dlseg; fold dlseg.
    Exists next.
    entailer!.
  - unfold dlseg; fold dlseg.
    Intros p'.
    Exists next'.
    Exists p'.
    entailer!. Qed.

(* Element to the left doubly-linked segment *)
Lemma dlseg_right_elem : forall sigma h head tail next prev,
    dlseg (sigma ++ [h]) head tail next prev |--
          EX q', !!(tail = h) && emp *
                   dlseg sigma head q' h prev *
                   data_at Tsh t_cds_list (next, q') h.
Proof.
  intros.
  revert head tail next prev.
  induction sigma; intros.
  - autorewrite with sublist.
    sep_apply dlseg_singleton.
    Exists prev.
    unfold dlseg; fold dlseg.
    entailer!.
  - unfold dlseg; fold dlseg.
    simpl.
    Intros p'.
    specialize (IHsigma p' tail next head).
    sep_apply IHsigma.
    Intros q'.
    Exists q' p'.
    entailer!. Qed.

Lemma elem_right_dlseg : forall sigma h head tail next prev prev',
    !!(tail = h) && emp *
      dlseg sigma head prev' h prev *
      data_at Tsh t_cds_list (next, prev') h |--
              dlseg (sigma ++ [h]) head tail next prev.
Proof.
  intros.
  revert head tail next prev prev'.
  induction sigma; intros.
  - simpl.
    Exists next.
    entailer!.
  - unfold dlseg; fold dlseg.
    Intros p'.
    specialize (IHsigma p' tail next head prev').
    assert_PROP (dlseg sigma p' prev' h head *
                data_at Tsh t_cds_list (next, prev') h |--
                !!(tail = h) && emp *
                dlseg sigma p' prev' h head *
                data_at Tsh t_cds_list (next, prev') h).
    { entailer!. }
    sep_apply H1.
    sep_apply IHsigma.
    { exact H. }
    simpl.
    Exists p'.
    entailer!. Qed.

(* Middle-element doubly-linked segment *)
Lemma dlseg_middle_elem : forall sigma1 h sigma2 head tail next prev,
    dlseg (sigma1 ++ h :: sigma2) head tail next prev |--
          EX next' prev',
             dlseg sigma1 head prev' h prev *
               data_at Tsh t_cds_list (next', prev') h *
               dlseg sigma2 next' tail next h.
Proof.
  intros.
  revert head tail next prev.
  induction sigma1, sigma2; intros; autorewrite with sublist; unfold dlseg; fold dlseg.
  - Intros next'.
    Exists next prev.
    entailer!.
  - Intros next'.
    Exists next' prev.
    entailer!.
  - assert ((a :: sigma1) ++ [h] = a :: (sigma1 ++ [h])).
    list_solve.
    rewrite H.
    pose proof dlseg_left_elem.
    specialize (H0 (sigma1 ++ [h]) a head tail next prev).
    sep_apply H0.
    Intros next'.
    specialize (IHsigma1 next' tail next a).
    sep_apply IHsigma1.
    Intros next'0 prev'.
    Exists next'0 prev'.
    Exists next'.
    unfold dlseg; fold dlseg.
    entailer!.
  - pose proof dlseg_left_elem.
    assert ((a :: sigma1) ++ h :: v :: sigma2 = a :: sigma1 ++ h :: v :: sigma2).
    list_solve.
    rewrite H0; clear H0.
    specialize (H (sigma1 ++ h :: v :: sigma2) a head tail next prev).
    sep_apply H.
    Intros next'.
    specialize (IHsigma1 next' tail next a).
    sep_apply IHsigma1.
    Intros next'0 prev'.
    Exists next'0 prev'.
    Exists next'.
    pose proof dlseg_left_elem.
    specialize (H1 sigma2 v next'0 tail next h).
    sep_apply H1.
    Intros next'1.
    Exists next'1.
    entailer!. Qed.

(* Lemma elem_middle_dlseg : forall sigma1 sigma2 h head_ tail_ next prev next' prev', *)
(*     dlseg sigma1 head_ prev' h prev * *)
(*       data_at Tsh t_cds_list (next', prev') h * *)
(*       dlseg sigma2 next' tail_ next h |-- *)
(*             dlseg (sigma1 ++ h :: sigma2) head_ tail_ next prev. *)
(* Proof. *)
(*   intros. *)
(*   revert head_ tail_ next prev next' prev' h. *)
(*   induction sigma1, sigma2; intros; autorewrite with sublist; unfold dlseg; fold dlseg. *)
(*   - Exists next'. *)
(*     entailer!. *)
(*   - Intros p'. *)
(*     Exists next' p'. *)
(*     entailer!. *)
(*   - assert ((a :: sigma1) ++ [h] = a :: (sigma1 ++ [h])). *)
(*     list_solve. *)
(*     rewrite H. *)
(*     pose proof elem_left_dlseg. *)
(*     specialize (H0 (sigma1 ++ [h]) a head_ tail_ next prev). *)
(*     Intros p'. *)
(*     specialize (H0 p'). *)
(*     assert_PROP (data_at Tsh t_cds_list (p', prev) head_ * *)
(*                 dlseg sigma1 p' prev' h head_ * *)
(*                 data_at Tsh t_cds_list (next', prev') h * emp |-- *)
(*                 !!(head_ = a) && emp * *)
(*                 dlseg (sigma1 ++ [h]) p' tail_ next a * *)
(*                 data_at Tsh t_cds_list (p', prev) a). *)
(*     + entailer!. *)
(*       cancel. *)
(*       specialize (IHsigma1 p' h head_ prev' next h next). *)
(*       assert_PROP (dlseg sigma1 p' prev' h head_ * *)
(*                   data_at Tsh t_cds_list (next, prev') h |-- *)
(*                   dlseg sigma1 p' prev' h head_ * *)
(*                   data_at Tsh t_cds_list (next, prev') h * *)
(*                   dlseg [] next h next h). *)
(*       * unfold dlseg; fold dlseg. *)
(*         entailer!. *)
(*         entailer!. *)
(*       * sep_apply H1. *)
(*         sep_apply IHsigma1. *)
(*         entailer!. *)
(*     + sep_apply H4. *)
(*       sep_apply H0. *)
(*       exact H3. *)
(*       entailer!. *)
(*   - Abort. *)

Lemma dlseg_local_facts_head : forall sigma head tail prev,
    dlseg sigma head tail nullval prev |--
          !!(is_pointer_or_null head) && emp *
      dlseg sigma head tail nullval prev.
Proof.
  intros.
  destruct sigma.
  - unfold dlseg.
    entailer!.
  - unfold dlseg; fold dlseg.
    Intros p'.
    Exists p'.
    entailer!. Qed.

Lemma dlseg_local_facts_tail_empty : forall head tail h,
    dlseg (@nil val) head tail h nullval |--
          !!(is_pointer_or_null tail) && emp *
      dlseg (@nil val) head tail h nullval.
Proof.
  unfold dlseg.
  { entailer!. } Qed.

Lemma dlseg_local_facts_tail_not_empty : forall sigma head tail next prev,
    sigma <> (@nil val) -> dlseg sigma head tail next prev |--
                                 !!(is_pointer_or_null tail) && emp *
                             dlseg sigma head tail next prev.
Proof.
  intros.
  revert head tail next prev.
  induction sigma.
  - contradiction.
  - unfold dlseg; fold dlseg.
    intros.
    Intros p'.
    Exists p'.
    pose proof classic (sigma <> []).
    destruct H1.
    + assert (forall head tail next prev : val,
             dlseg sigma head tail next prev |--
             !!(is_pointer_or_null tail) && emp *
             dlseg sigma head tail next prev).
      tauto.
      specialize (H2 p' tail next head).
      sep_apply H2.
      entailer. entailer!.
    + assert (sigma = []).
      tauto.
      rewrite H2.
      unfold dlseg.
      entailer!. Qed.

Lemma dlseg_null_tail : forall sigma head,
    dlseg sigma head nullval nullval nullval |--
          !!(sigma = [] /\ head = nullval).
Proof.
  intros.
  pose proof classic (sigma <> []).
  destruct H.
  - pose proof exists_last.
    specialize (X _ sigma).
    apply X in H.
    clear X.
    destruct H. destruct s.
    rewrite e.
    sep_apply dlseg_right_elem.
    Intros p'.
    subst.
    entailer!.
  - assert (sigma = []).
    tauto.
    rewrite H0.
    unfold dlseg.
    entailer!. Qed.

(*********************************************************************)
(* `cds_list_head` representation                                    *)
(*                                                                   *)
Definition cds_list_rep (sigma : list val) (p : val) : mpred :=
  EX (next prev : val),
     dlseg sigma p p next prev.

(* Prevent automatic [unfold] of [simpl] *)
Arguments cds_list_rep sigma p : simpl never.

Theorem dlseg_cds_list_rep : forall sigma p next prev,
    dlseg sigma p p next prev |--
      cds_list_rep sigma p.
Proof.
  intros. unfold cds_list_rep.
  entailer!.
  Exists next.
  Exists prev.
  entailer!. Qed.


(**
 *)

Definition cds_list_add_spec : ident * funspec :=
  DECLARE _cds_list_add
   WITH sigma1 : list val, p1 : val, sigma2 : list val, p2 : val
   PRE [ tptr t_cds_list, tptr t_cds_list ]
      PROP () PARAMS (p1; p2) SEP (cds_list_rep sigma1 p1; cds_list_rep sigma2 p2)
   POST [ tvoid ]
      PROP () RETURN () SEP (cds_list_rep (sigma1 ++ sigma2) p1).

Definition cds_list_add_tail_spec : ident * funspec :=
  DECLARE _cds_list_add_tail
   WITH sigma1 : list val, p1 : val, sigma2 : list val, p2 : val
   PRE [ tptr t_cds_list, tptr t_cds_list ]
      PROP () PARAMS (p1; p2) SEP (cds_list_rep sigma1 p1; cds_list_rep sigma2 p2)
   POST [ tvoid ]
      PROP () RETURN () SEP (cds_list_rep (sigma2 ++ sigma1) p2).

Definition __cds_list_del_spec : ident * funspec :=
  DECLARE ___cds_list_del
   WITH sigma : list val, p : val, q : val
   PRE [ tptr t_cds_list, tptr t_cds_list ]
      PROP () PARAMS (p; q) SEP (cds_list_rep sigma p ; cds_list_rep sigma q)
   POST [ tvoid ]
     EX x : Z,
      PROP () RETURN () SEP (cds_list_rep (remove_Znth x sigma) p).

Definition cds_list_del_spec : ident * funspec :=
  DECLARE _cds_list_del
   WITH sigma : list val, p : val
   PRE [ tptr t_cds_list ]
      PROP () PARAMS (p) SEP (cds_list_rep sigma p)
   POST [ tvoid ]
     EX x : Z,
      PROP () RETURN () SEP (cds_list_rep (remove_Znth x sigma) p).

Definition cds_list_move_spec : ident * funspec :=
  DECLARE _cds_list_move
   WITH sigma1 : list val, p1 : val, sigma2 : list val, p2 : val
   PRE [ tptr t_cds_list, tptr t_cds_list ]
      PROP () PARAMS (p1; p2) SEP (cds_list_rep sigma1 p1; cds_list_rep sigma2 p2)
   POST [ tvoid ]
      PROP () RETURN () SEP (cds_list_rep nil p1; cds_list_rep (sigma1 ++ sigma2) p1).

Definition cds_list_replace_spec : ident * funspec :=
  DECLARE _cds_list_replace
   WITH sigma1 : list val, p1 : val, sigma2 : list val, p2 : val
   PRE [ tptr t_cds_list, tptr t_cds_list ]
      PROP () PARAMS (p1; p2) SEP (cds_list_rep sigma1 p1; cds_list_rep sigma2 p2)
   POST [ tvoid ]
      PROP () RETURN () SEP (cds_list_rep nil p1; cds_list_rep sigma2 p1).

Definition list_splice_head {A : Type} (l1 : list A) (l2 : list A) : list A :=
  match l2 with
  | nil => l1
  | h :: hs => h :: l1 ++ hs
  end.

Definition cds_list_splice_spec : ident * funspec :=
  DECLARE _cds_list_splice
   WITH sigma1 : list val, p1 : val, sigma2 : list val, p2 : val
   PRE [ tptr t_cds_list, tptr t_cds_list ]
      PROP () PARAMS (p1; p2) SEP (cds_list_rep sigma1 p1; cds_list_rep sigma2 p2)
   POST [ tvoid ]
     EX p' : val,
      PROP () RETURN () SEP (cds_list_rep (list_splice_head sigma1 sigma2) p';
                             cds_list_rep (list_splice_head sigma1 sigma2) p2).

Definition empty_fn_model (sigma : list val) : Z :=
  match sigma with
  | nil => Z.one
  | _ => Z.zero
  end.

Definition cds_list_empty_spec : ident * funspec :=
  DECLARE _cds_list_empty
   WITH sigma : list val, p : val
   PRE [ tptr t_cds_list ]
      PROP () PARAMS (p) SEP (cds_list_rep sigma p)
   POST [ tint ]
      PROP () RETURN (Vint (Int.repr (empty_fn_model sigma)))
      SEP (cds_list_rep sigma p).


(* Global function specs

 NOTE: all go here
 *)
Definition Gprog : funspecs := [ cds_list_add_spec; cds_list_add_tail_spec; __cds_list_del_spec; cds_list_del_spec;
                                 cds_list_move_spec; cds_list_replace_spec; cds_list_splice_spec; cds_list_empty_spec ].


Lemma body_cds_list_add : semax_body Vprog Gprog f_cds_list_add cds_list_add_spec.
Proof.
  start_function.
  Abort.

Lemma body_cds_list_add_tail : semax_body Vprog Gprog f_cds_list_add_tail cds_list_add_tail_spec.
Proof.
  start_function.
  Abort.

Lemma body___cds_list_del : semax_body Vprog Gprog f___cds_list_del __cds_list_del_spec.
Proof.
  start_function.
  Abort.

Lemma body_cds_list_del : semax_body Vprog Gprog f_cds_list_del cds_list_del_spec.
Proof.
  start_function.
  Abort.

Lemma body_cds_list_move : semax_body Vprog Gprog f_cds_list_move cds_list_move_spec.
Proof.
  start_function.
  Abort.

Lemma body_cds_list_replace : semax_body Vprog Gprog f_cds_list_replace cds_list_replace_spec.
Proof.
  start_function.
  Abort.

Lemma body_cds_list_splice : semax_body Vprog Gprog f_cds_list_splice cds_list_splice_spec.
Proof.
  start_function.
  Abort.

Lemma body_cds_list_empty : semax_body Vprog Gprog f_cds_list_empty cds_list_empty_spec.
Proof.
  start_function.
  Abort.
